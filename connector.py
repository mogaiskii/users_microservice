from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class Connector:
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Connector, cls).__new__(cls)
        return cls.instance

    def set_connection(self, db_url):
        # TODO: manage connections?
        if '_engine' not in self.__dict__:
            self._engine = create_engine(db_url)
            self._session_maker = sessionmaker(bind=self._engine)
            self._session = self._session_maker()
        else:
            self._session = self._session_maker()

    @property
    def session(self):
        return self._session

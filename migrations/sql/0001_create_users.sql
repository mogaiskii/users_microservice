CREATE TABLE IF NOT EXISTS users (
  id SERIAL UNIQUE,
  username varchar (255) UNIQUE NOT NULL,
  email varchar (255) UNIQUE NOT NULL,
  password_encoded varchar (255) NOT NULL
);
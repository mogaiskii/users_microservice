CREATE TABLE IF NOT EXISTS offers (
  id SERIAL UNIQUE,
  user_id integer NOT NULL,
  title varchar (255) NOT NULL,
  text text,
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);
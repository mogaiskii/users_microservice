import os
import unittest
import json
from dotenv import load_dotenv

from app import app
from connector import Connector
from models import User, Offer
from auth_util import get_token


class RegistryTestCase(unittest.TestCase):
    def setUp(self):
        load_dotenv()
        db_url = os.getenv('TEST_DB_URL')
        Connector().set_connection(db_url)

    def test_alive(self):
        try:
            request, response = app.test_client.post(
                '/user/registry',
                data=json.dumps({
                    'username': 'test1',
                    'email': 'test1@test1',
                    'password': 'test1'
                })
            )
            self.assertEqual(response.status, 201)
        except:
            self.assertTrue(False)

    def test_uniq_fields_violation_causes_error(self):
        try:
            app.test_client.post(
                '/user/registry',
                data=json.dumps({
                    'username': 'test2',
                    'email': 'test1@test2',
                    'password': 'test1'
                })
            )

            request, response = app.test_client.post(
                '/user/registry',
                data=json.dumps({
                    'username': 'test2',
                    'email': 'test1@test2',
                    'password': 'test1'
                })
            )
            self.assertEqual(response.status, 400)
        except expression as identifier:
            self.assertTrue(False)

    def tearDown(self):
        session = Connector().session
        session.query(Offer).delete()
        session.query(User).delete()
        session.commit()


class AuthTestCase(unittest.TestCase):
    def setUp(self):
        load_dotenv()
        db_url = os.getenv('TEST_DB_URL')
        Connector().set_connection(db_url)
        session = Connector().session
        user = User('test', 'test@test.com', 'password123')
        session.add(user)
        session.commit()

    def test_alive(self):
        try:
            request, response = app.test_client.post(
                '/user/auth',
                data=json.dumps({
                    'username': 'test',
                    'password': 'password123'
                })
            )
            self.assertEqual(response.status, 200)
        except:
            self.assertTrue(False)

    def test_password_validation(self):
        try:
            request, response = app.test_client.post(
                '/user/auth',
                data=json.dumps({
                    'username': 'test',
                    'password': 'wrong'
                })
            )
            self.assertEqual(response.status, 401)
        except:
            self.assertTrue(False)

    def tearDown(self):
        session = Connector().session
        session.query(Offer).delete()
        session.query(User).delete()
        session.commit()


class UserTestCase(unittest.TestCase):
    def setUp(self):
        load_dotenv()
        db_url = os.getenv('TEST_DB_URL')
        Connector().set_connection(db_url)
        self.self.session = Connector().session

        self.user = User('test', 'test@test.com', 'password123')
        self.session.add(self.user)
        self.session.commit()

        # only after commit user has id!
        self.token = get_token(self.user)

    def test_alive(self):
        try:
            request, response = app.test_client.get(
                '/user/%s' % self.user.id,
                headers={
                    'authorization': self.token
                }
            )
            self.assertEqual(response.status, 200)
        except:
            self.assertTrue(False)

    def test_user_returns_correctly(self):
        try:
            request, response = app.test_client.get(
                '/user/%s' % self.user.id,
                headers={
                    'authorization': self.token
                }
            )

            response_body = json.loads(response.body)
            real = response_body['user']
            expected = {
                'id': self.user.id,
                'username': self.user.username,
                'email': self.user.email
            }
            self.assertEqual(real, expected)
        except:
            self.assertTrue(False)

    def test_offers_returns_correctly(self):
        try:
            # clear offers table before that test
            self.session.query(Offer).delete()
            self.session.commit()

            offer = Offer(self.user, 'title', 'text')
            self.session.add(offer)
            self.session.commit()

            request, response = app.test_client.get(
                '/user/%s' % self.user.id,
                headers={
                    'authorization': self.token
                }
            )

            response_body = json.loads(response.body)
            real = response_body['offers']

            expected = [{
                'id': offer.id,
                'user_id': self.user.id,
                'title': offer.title,
                'text': offer.text
            }]
            self.assertEqual(real, expected)
        except:
            self.assertTrue(False)

    def tearDown(self):
        session = Connector().session
        session.query(Offer).delete()
        session.query(User).delete()
        session.commit()

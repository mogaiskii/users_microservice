import os
import unittest
import dotenv

from connector import Connector
from models import User, Offer

# TODO: Not tears down sometimes


class UserTestCase(unittest.TestCase):
    def setUp(self):
        dotenv.load_dotenv()
        db_url = os.getenv('TEST_DB_URL')
        Connector().set_connection(db_url)
        self.session = Connector().session

    def test_create_and_get(self):
        user1 = User('test', 'test@test.com', 'password123')
        try:
            self.session.add(user1)
            self.session.commit()

            user2 = self.session.query(User).filter_by(username='test').first()
            self.assertTrue(user2.username == 'test')
        except:
            self.assertTrue(False)

    def tearDown(self):
        self.session.query(User).delete()
        self.session.commit()


class OfferTestCase(unittest.TestCase):
    def setUp(self):
        dotenv.load_dotenv()
        db_url = os.getenv('TEST_DB_URL')
        Connector().set_connection(db_url)
        self.session = Connector().session

        self.user = User('test', 'test@test.com', 'password123')
        self.session.add(self.user)
        self.session.commit()

    def test_create_and_get(self):
        offer1 = Offer(self.user, 'title', 'text')
        try:
            self.session.add(offer1)
            self.session.commit()

            offer2 = self.session.query(Offer).filter_by(title='title').first()
            self.assertTrue(offer2.text == 'text')
        except:
            self.assertTrue(False)

    def tearDown(self):
        self.session.query(Offer).delete()
        self.session.query(User).delete()
        self.session.commit()

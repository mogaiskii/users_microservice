import os
import json
from sanic import Sanic
from sanic.response import json as json_response, raw as raw_response
from dotenv import load_dotenv
from sqlalchemy import or_
from connector import Connector
from models import User, Offer
from auth_util import get_token, authenticate

app = Sanic()


@app.route('/user/registry', methods=['POST'])
async def registry(request):
    try:
        request_data = json.loads(request.body)
        username = request_data['username']
        email = request_data['email']
        password = request_data['password']

        session = Connector().session
        similar = session.query(User).filter(
            or_(
                User.username == username,
                User.email == email
                )
        ).limit(1).count()
        if similar:
            return raw_response(b'', status=400)

        user = User(username, email, password)
        session.add(user)
        session.commit()
        jwt_token = get_token(user)

        return json_response(
            {
                'id': user.id,
                'username': username,
                'email': email,
                'token': jwt_token
            },
            status=201
        )
    except (json.decoder.JSONDecodeError, AttributeError):
        return raw_response(b'', status=400)


@app.route('/user/auth', methods=['POST'])
async def auth(request):
    try:
        request_data = json.loads(request.body)
        username = request_data['username']
        password = request_data['password']

        session = Connector().session
        user = session.query(User).filter_by(username=username).first()
        if not user:
            return raw_response(b'', status=404)
        assert user.password_verify(password)

        jwt_token = get_token(user)

        return json_response(
            {
                'id': user.id,
                'token': jwt_token
            },
            status=200
        )
    except json.decoder.JSONDecodeError:
        return raw_response(b'', status=400)
    except AssertionError:
        return raw_response(b'', status=401)


@app.route('/user/<user_id:int>', methods=['GET'])
async def user(request, user_id):
    request_user = authenticate(request)
    if not request_user or request_user.id != user_id:
        return raw_response(b'', status=401)

    session = Connector().session
    offers = session.query(Offer).filter_by(
        user_id=request_user.id
        ).all()

    return json_response(
        {
            'user': {
                'id': user_id,
                'username': request_user.username,
                'email': request_user.email
            },
            'offers': [
                {
                    'id': offer.id,
                    'user_id': offer.user_id,
                    'title': offer.title,
                    'text': offer.text
                } for offer in offers if offer
            ]
        },
        status=200
    )


if __name__ == '__main__':
    load_dotenv()
    db_url = os.getenv('DB_URL')
    Connector().set_connection(db_url)
    port = os.getenv('APP_PORT', 8000)
    app.run(host="0.0.0.0", port=port)

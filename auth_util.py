import os
import jwt
from datetime import datetime, timedelta
from sanic.response import json as json_response, raw as raw_response
from connector import Connector
from models import User

JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_SECONDS = 60*60*24


def get_token(user):
    token_payload = {
            'user_id': user.id,
            'exp': datetime.utcnow() + timedelta(
                seconds=JWT_EXP_DELTA_SECONDS
                )
        }
    jwt_secret = os.getenv('JWT_SECRET')
    jwt_token = jwt.encode(token_payload, jwt_secret, JWT_ALGORITHM)
    return jwt_token.decode()


def authenticate(request):
    jwt_token = request.headers.get('authorization', None)
    if jwt_token:
        jwt_secret = os.getenv('JWT_SECRET')
        try:
            payload = jwt.decode(
                jwt_token, jwt_secret,
                algorithms=[JWT_ALGORITHM]
            )
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return json_response({'message': 'Token is invalid'}, status=400)
        session = Connector().session
        user = session.query(User).get(payload['user_id'])
        return user

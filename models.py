import bcrypt
from sqlalchemy import Column, Integer, String, ForeignKey, Text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(255), nullable=False, unique=True)
    email = Column(String(255), nullable=False, unique=True)
    password_encoded = Column(String(255), nullable=False)

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password

    @property
    def password(self):
        raise AttributeError('password not readable')

    @password.setter
    def password(self, password):
        self.password_encoded = bcrypt.hashpw(
            password.encode('utf-8'),
            bcrypt.gensalt()
        ).decode()

    def password_verify(self, password):
        return bcrypt.checkpw(
            password.encode('utf-8'),
            self.password_encoded.encode('utf-8')
            )


class Offer(Base):
    __tablename__ = 'offers'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User")
    title = Column(String(255), nullable=False)
    text = Column(Text)

    def __init__(self, user, title, text):
        self.user = user
        self.title = title
        self.text = text
